<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\ReviewRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class BookController
 * @package App\Controller
 * @Route("/api/reviews", name="review_")
 */
class ReviewController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     * @param ReviewRepository $reviewRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function index(ReviewRepository $reviewRepository, SerializerInterface $serializer): JsonResponse
    {
        $reviews = $reviewRepository->findAll();

        $jsonReviews = $serializer->serialize($reviews, 'json');

        return new JsonResponse($jsonReviews, 200, [], true);
    }

    /**
     * @Route("/{id}/", name="show_review", methods={"GET"})
     * @param int $id
     * @param ReviewRepository $reviewRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function show(int $id, ReviewRepository $reviewRepository, SerializerInterface $serializer): JsonResponse
    {
        $book = $reviewRepository->find($id);
        $json = $serializer->serialize($book, 'json');

        return new JsonResponse($json, 200, [], true);
    }
}
