<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use App\Repository\ReviewRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class BookController
 * @package App\Controller
 * @Route("/api/books", name="book_")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     * @param BookRepository $bookRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function index(BookRepository $bookRepository, SerializerInterface $serializer): JsonResponse
    {
        $books = $bookRepository->findAll();

        $jsonBooks = $serializer->serialize($books, 'json', ["groups" => ["visitor"]]);

        return new JsonResponse($jsonBooks, 200, [], true);
    }

    /**
     * @Route("/{id}/", name="show_book", methods={"GET"})
     * @param int $id
     * @param BookRepository $bookRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function show(int $id, BookRepository $bookRepository, SerializerInterface $serializer): JsonResponse
    {
        $book = $bookRepository->find($id);
        $json = $serializer->serialize($book, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{id}/reviews", name="show_reviews_by_book", methods={"GET"})
     * @param int $id
     * @param ReviewRepository $reviewRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function showReviewsByBook(int $id, ReviewRepository $reviewRepository, SerializerInterface $serializer): JsonResponse
    {
        $reviews = $reviewRepository->findByBook($id);
        $json = $serializer->serialize($reviews, 'json');

        return new JsonResponse($json, 200, [], true);
    }

//    /**
//     * @param Request $request
//     * @return JsonResponse
//     * @Route("/new", name="new_book", methods={"POST"})
//     */
//    public function new(Request $request): JsonResponse
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $book = new Book();
//
//        $form = $this->createForm(BookType::class, $book);
//        $form->submit($request->request->all());
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em->persist($book);
//            $em->flush();
//
//            return new JsonResponse(['message' => 'Book has been created'], 200);
//        } else {
//            return new JsonResponse(['message' => 'Wrong data in form'], 404);
//        }
//    }*/
}
