<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     shortName="Livre",
 *     subresourceOperations={
 *          "book_get_subresource"={
 *              "method"="GET",
 *              "route"="show_reviews_by_book",
 *          },
 *      },
 *     itemOperations={
 *          "delete_book"= {
 *              "method" = "DELETE",
 *              "path" = "livres/{id}",
 *              "requirements"={"id"="\d+"},
 *          },
 *          "update_book"= {
 *              "method" = "PUT",
 *              "path" = "livres/{id}",
 *              "requirements"={"id"="\d+"},
 *          },
 *          "show_book"= {
 *              "method"="GET",
 *              "route"="show_book",
 *              "requirements"={"id"="\d+"},
 *              "swagger_context"={
 *                  "summary"="Find one book by id",
 *                  "description"="
                            # Book
                            ![A book](https://zupimages.net/up/19/31/5cn5.png)
                            Get data from the *selected* book with the **id** ",
 *                  "parameters" = {
 *                    {
 *                        "name" = "id",
 *                        "in" = "path",
 *                        "required" = true,
 *                        "type" = "integer",
 *                        "description"="Book Id"
 *                    }
 *                },
 *                  "responses" = {
 *                      "200" = {
 *                          "description" = "Data from the selected book",
 *                          "schema" =  {
 *                              "type" = "object",
 *                              "properties" = {
 *                                   "id" = {
 *                                      "type" = "integer",
 *                                      "example" = 1
 *                                   },
 *                                   "isbn" = {
 *                                      "type" = "string",
 *                                      "example" = "9788932918372"
 *                                   },
 *                                   "title" = {
 *                                      "type" = "string",
 *                                      "example" = "Lord of the ring"
 *                                   },
 *                                   "reviews" = {
 *                                      "type" = "object",
 *                                      "example" = "/api/avis/1"
 *                                   },
 *                                   "author" = {
 *                                      "type" = "string",
 *                                      "example" = "Stephen King"
 *                                   },
 *                                   "publishedAt" = {
 *                                      "type" = "date",
 *                                      "example" = "2019-07-05"
 *                                   },
 *                                   "description" = {
 *                                      "type" = "string",
 *                                      "example" = "Il s'agit d'un roman de xxx publié en l'occasion de.."
 *                                   }
 *                              }
 *                          }
 *                      },
 *                      "400" = {
 *                          "description" = "Invalid input"
 *                      },
 *                      "404" = {
 *                          "description" = "Book not found"
 *                      }
 *                   },
 *              },
 *     },
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"admin", "visitor"})
     */
    private $id;

    /**
     * @Groups("publication")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Isbn()
     * @Groups({"visitor", "admin"})
     */
    private $isbn;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"visitor", "admin"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"admin", "visitor"})
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     * @Groups({"visitor", "admin"})
     */
    private $publishedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Review", mappedBy="book")
     * @Groups({"visitor", "admin"})
     * @ApiSubresource()
     */
    private $reviews;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"visitor", "admin"})
     */
    private $description;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setBook($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->contains($review)) {
            $this->reviews->removeElement($review);
            // set the owning side to null (unless already changed)
            if ($review->getBook() === $this) {
                $review->setBook(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
