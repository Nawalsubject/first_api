<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     shortName="Avis",
 *     itemOperations={
 *          "show_review"= {
 *              "method"="GET",
 *              "route"="show_review",
 *              "requirements"={"id"="\d+"},
 *              "swagger_context"={
 *                  "summary"="Find one review by id",
 *                  "description"="
# Review
![A review](https://zupimages.net/up/19/31/xzk2.png)
Get data from the *selected* review with the **id** ",
 *                  "parameters" = {
 *                    {
 *                        "name" = "id",
 *                        "in" = "path",
 *                        "required" = "true",
 *                        "type" = "integer",
 *                        "description"="Review Id"
 *                    }
 *                },
 *                  "responses" = {
 *                      "200" = {
 *                          "description" = "Data from the selected review",
 *                          "schema" =  {
 *                              "type" = "object",
 *                              "properties" = {
 *                                   "id" = {
 *                                      "type" = "integer",
 *                                      "example" = 1
 *                                   },
 *                                   "rating" = {
 *                                      "type" = "integer",
 *                                      "example" = "0 to 5"
 *                                   },
 *                                   "body" = {
 *                                      "type" = "string",
 *                                      "example" = "This is a very nice book."
 *                                   },
 *                                   "author" = {
 *                                      "type" = "string",
 *                                      "example" = "John Doe"
 *                                   },
 *                                   "publishedAt" = {
 *                                      "type" = "date",
 *                                      "example" = "2019-07-05"
 *                                   },
 *                                   "book" = {
 *                                      "type" = "object",
 *                                      "example" = "/api/livre/1"
 *                                   }
 *                              }
 *                          }
 *                      },
 *                      "400" = {
 *                          "description" = "Invalid input"
 *                      },
 *                      "404" = {
 *                          "description" = "Review not found"
 *                      }
 *                   },
 *              },
 *           },
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Range(min="0", max="5")
     */
    private $rating;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $body;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     */
    private $publishedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book", inversedBy="reviews")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $book;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }
}
