import React from 'react';
import RichTextInput from 'ra-input-rich-text';
import { HydraAdmin } from '@api-platform/admin';
import parseHydraDocumentation from '@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation';

const entrypoint = 'http://127.0.0.1:8000/api';

const myApiDocumentationParser = entrypoint => parseHydraDocumentation(entrypoint)
    .then( ({ api }) => {

        const books = api.resources.find(({ name }) => 'books' === name);
        const description = books.fields.find(({ name }) => 'description' === name);

        description.input = props => (
            <RichTextInput {...props} source="description" />
        );

        description.input.defaultProps = {
            addField: true,
            addLabel: true,
        };

        return { api };
    })
;

export default (props) => <HydraAdmin apiDocumentationParser={myApiDocumentationParser} entrypoint={entrypoint}/>;